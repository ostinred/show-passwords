window.onload = function() {
  const checkboxSolo = document.querySelector('#check1');
  const checkboxAll = document.querySelector('#check2');

  var showPass = function() {
    const formId = this.closest('form').id;
    document
      .querySelectorAll(`#${formId} .form-password`)
      .forEach(x => x.setAttribute('type', this.checked ? 'text' : 'password'));
  };

  checkboxSolo.addEventListener('change', showPass);
  checkboxAll.addEventListener('change', showPass);
};
